package com.xueyuan.wata.daph.node.jvm.httpresult.simple.transformer

import com.xueyuan.wata.daph.jvm.api.node.httpresult.{HttpResult, HttpResultMultipleTransformer}
import com.xueyuan.wata.daph.node.jvm.httpresult.{HttpConfig, http}

class HttpTransformer extends HttpResultMultipleTransformer {
  override protected def transform(lineToDS: Map[String, HttpResult]): HttpResult = {
    val config = nodeConfig.asInstanceOf[HttpConfig]

    val res = http(config)

    val message = res.message
    val code = res.code
    logger.info(s"Node[$id]Http success! Code is $code")

    res
  }

  override def getNodeConfigClass = classOf[HttpConfig]
}
