package com.xueyuan.wata.daph.node.jvm

import com.xueyuan.wata.daph.jvm.api.node.httpresult.HttpResult
import org.apache.logging.log4j.scala.Logging

import scala.collection.JavaConverters._

package object httpresult extends Logging {
  def http(config: HttpConfig): HttpResult = {
    val url = config.url
    val method = config.method
    val headers = config.headers.asJava
    val params = config.params.asJava

    val res = HttpMethodType.withName(method) match {
      case HttpMethodType.GET => HttpUtils.doGet(url, headers, params)
      case HttpMethodType.POST => HttpUtils.doPost(url, headers, params)
      case HttpMethodType.PUT => HttpUtils.doPut(url, params)
      case HttpMethodType.DELETE => HttpUtils.doDelete(url, params)
    }

    res
  }
}
