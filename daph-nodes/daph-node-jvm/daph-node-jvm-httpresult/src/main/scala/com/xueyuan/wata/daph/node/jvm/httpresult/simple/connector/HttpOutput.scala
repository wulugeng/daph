package com.xueyuan.wata.daph.node.jvm.httpresult.simple.connector

import com.xueyuan.wata.daph.jvm.api.node.httpresult.{HttpResult, HttpResultSingleOutput}
import com.xueyuan.wata.daph.node.jvm.httpresult.{HttpConfig, http}

class HttpOutput extends HttpResultSingleOutput {
  override protected def out(ds: HttpResult): Unit = {
    val config = nodeConfig.asInstanceOf[HttpConfig]

    val res = http(config)

    val message = res.message
    val code = res.code
    logger.info(s"Node[$id]Http success! Code is $code")
  }

  override def getNodeConfigClass = classOf[HttpConfig]
}
