package com.xueyuan.wata.daph.node.spark3.dataframe.batch.connector.hive

import com.xueyuan.wata.daph.api.config.NodeConfig

case class HiveInputConfig(
  sql: String
) extends NodeConfig
