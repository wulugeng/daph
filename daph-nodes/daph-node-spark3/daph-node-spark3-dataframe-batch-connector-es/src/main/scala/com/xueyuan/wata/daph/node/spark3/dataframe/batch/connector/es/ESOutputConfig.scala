package com.xueyuan.wata.daph.node.spark3.dataframe.batch.connector.es

import com.xueyuan.wata.daph.api.config.NodeConfig

case class ESOutputConfig(
  resource: String,
  cfg: Map[String, String]
) extends NodeConfig