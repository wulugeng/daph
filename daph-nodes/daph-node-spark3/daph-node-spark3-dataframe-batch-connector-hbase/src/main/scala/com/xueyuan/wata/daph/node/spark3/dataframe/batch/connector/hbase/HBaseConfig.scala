package com.xueyuan.wata.daph.node.spark3.dataframe.batch.connector.hbase

import com.xueyuan.wata.daph.api.config.NodeConfig

class HBaseConfig(
  val catalog: String,
  val hbaseOptions: Map[String, String]
) extends NodeConfig

case class HBaseOutputConfig(
  stagingDir: String,
  saveMode: String,
  override val catalog: String,
  override val hbaseOptions: Map[String, String]
) extends HBaseConfig(catalog, hbaseOptions)