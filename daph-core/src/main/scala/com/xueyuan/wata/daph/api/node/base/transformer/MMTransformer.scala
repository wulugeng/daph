package com.xueyuan.wata.daph.api.node.base.transformer

abstract class MMTransformer[IN, OUT] extends Transformer {
	final override def run(): Unit = {
		val be = before(lineToDS)
		val tr = transform(be)
		val af = after(tr)
		af.foreach { case (ds, line) =>
			setDSToLine(line, ds)
		}
	}

	protected def before(lineToDS: Map[String, IN]): Map[String, IN] = lineToDS
	protected def transform(lineToDS: Map[String, IN]): Map[String, OUT]
	protected def after(lineToDS: Map[String, OUT]): Map[String, OUT] = lineToDS

	private def lineToDS = getLineToDS.map { case (lane, ds) =>
		lane -> ds.asInstanceOf[IN]
	}
}
