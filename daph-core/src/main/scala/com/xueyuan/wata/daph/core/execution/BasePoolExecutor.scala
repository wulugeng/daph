package com.xueyuan.wata.daph.core.execution

import com.xueyuan.wata.daph.api.exception.DaphException
import com.xueyuan.wata.daph.api.executor.AbstractPoolExecutor
import com.xueyuan.wata.daph.core.execution.task.Task

import scala.collection.mutable

class BasePoolExecutor(parallelism: Int) extends AbstractPoolExecutor(parallelism) {
  override protected def nameFormat: String = "BasePoolExecutor-pool-%d"

  override def execute(): Unit = {
    logger.info("BasePoolExecutor开始执行")

    val submittedTasks = mutable.HashSet[Task]()
    while (!getStatus) {
      val haveFailed = tasks.exists(_.context.isFailed)
      val allSucceed = tasks.forall(_.context.isSucceed)
      if (haveFailed | allSucceed) {
        setStatus(true)
      } else {
        val unSubmittedTasks = tasks.diff(submittedTasks)
        val succeedTasksInSubmitted = submittedTasks.filter(_.context.isSucceed)
        val unSubmittedTasksAllUpSucceed = unSubmittedTasks.filter { task =>
          _dag.getAllUpTasks(task).forall(succeedTasksInSubmitted.contains)
        }
        unSubmittedTasksAllUpSucceed.foreach { task =>
          if (!task.context.isSubmitted) {
            logger.info(s"Submit $task")

            pool.submit(new Runnable {
              override def run(): Unit = task.run()
            })
            task.context.markTaskSubmitted()
            logger.info(s"$task submitted")

            submittedTasks.add(task)
          }
        }
      }
    }

    val tasksFailed = tasks.filter(_.context.isFailed)
    if (tasksFailed.isEmpty) {
      logger.info("BasePoolExecutor执行成功")
    } else {
      tasksFailed.foreach { task =>
        val te = task.context.getException.get
        logger.error("Task运行失败", te)
        throw te
      }
      throw new DaphException(s"BasePoolExecutor执行失败")
    }
  }

  override def stop(): Unit = {
    pool.shutdownNow()
  }
}
