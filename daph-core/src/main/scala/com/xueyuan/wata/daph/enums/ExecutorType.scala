package com.xueyuan.wata.daph.enums

object ExecutorType extends Enumeration {
  val BASE_POOL = Value(name = "base-pool")
  val BASE_FUTURE = Value(name = "base-future")
  val DEFAULT = Value(name = "default")
}
