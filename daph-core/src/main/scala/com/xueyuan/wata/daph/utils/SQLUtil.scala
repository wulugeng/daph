package com.xueyuan.wata.daph.utils

import net.sf.jsqlparser.parser.CCJSqlParserManager
import net.sf.jsqlparser.util.TablesNamesFinder

import java.io.StringReader
import scala.jdk.CollectionConverters.collectionAsScalaIterableConverter

object SQLUtil {
  def getTableNames(sql: String): List[String] = {
    val parserManager = new CCJSqlParserManager()
    val statement = parserManager.parse(new StringReader(sql))
    val tablesNamesFinder = new TablesNamesFinder()
    val tableNames = tablesNamesFinder.getTableList(statement).asScala.toList
    tableNames
  }
}