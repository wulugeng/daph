package com.xueyuan.wata.daph.api.node.base.connector.input

import com.xueyuan.wata.daph.api.GlobalContext
import com.xueyuan.wata.daph.api.node.{Node, NodeDescription, NodeInitErrorResult}

abstract class Input[OUT] extends Node {
  override def init(nodeDescription: NodeDescription,
                    gc: GlobalContext): java.util.List[NodeInitErrorResult] = {
    val res = super.init(nodeDescription, gc)

    if (nodeDescription.outLines.isEmpty) {
      res.add(NodeInitErrorResult(
        this.getClass.getSimpleName,
        "outLines",
        "应当有输出线",
        s"${nodeDescription.outLines.mkString("Array(", ", ", ")")}"
      ))
    }

    res
  }

  final override def run(): Unit = {
    before()
    val ds = in()
    val res = after(ds)

    setDSToLines(res, nodeDescription.outLines)
  }

  protected def before(): Unit = {}

  protected def in(): OUT
  protected def after(out: OUT): OUT = out

  final override def viewNode(): Unit = {
    val res = view()
    setDSToLines(res, Array(nodeDescription.id))
  }

  def view(): OUT = {
    in()
  }
}
