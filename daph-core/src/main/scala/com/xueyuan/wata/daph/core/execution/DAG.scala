package com.xueyuan.wata.daph.core.execution

import com.xueyuan.wata.daph.core.execution.task.Task
import com.xueyuan.wata.daph.utils.DAGUtil.findChildren
import org.apache.commons.lang3.StringUtils
import org.apache.logging.log4j.scala.Logging

class DAG(tasks: Set[Task]) extends Logging {
  private val taskToUpTasks: Map[Task, Set[Task]] = tasks.map { task =>
    val inputLines = task.getNodeDescription.inLines.filter(StringUtils.isNotBlank).toSet
    val upTasks = tasks.filter { task =>
      val outputLines = task.getNodeDescription.outLines.filter(StringUtils.isNotBlank).toSet
      (inputLines & outputLines).nonEmpty
    }
    task -> upTasks
  }.toMap
  private val taskToDownTasks = tasks.map { task =>
    val outputLines = task.getNodeDescription.outLines.filter(StringUtils.isNotBlank).toSet
    val downTasks = tasks.filter { task =>
      val inputLines = task.getNodeDescription.inLines.filter(StringUtils.isNotBlank).toSet
      (inputLines & outputLines).nonEmpty
    }
    task -> downTasks
  }.toMap

  def getTasks: Set[Task] = tasks

  def getTaskToUpTasks: Map[Task, Set[Task]] = taskToUpTasks

  def getTaskToDownTasks: Map[Task, Set[Task]] = taskToDownTasks

  def getUpTasks(task: Task): Set[Task] = taskToUpTasks(task)

  def getDownTasks(task: Task): Set[Task] = taskToDownTasks(task)

  def getAllUpTasks(task: Task): Set[Task] = findChildren(task, taskToUpTasks)
}
