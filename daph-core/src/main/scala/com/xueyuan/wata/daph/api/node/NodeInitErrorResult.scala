package com.xueyuan.wata.daph.api.node

case class NodeInitErrorResult(
  nodeName: String,
  entryName: String = "",
  entryValue: String = "",
  message: String
) extends Serializable
