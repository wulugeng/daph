package com.xueyuan.wata.daph.constants

object GlobalConstants {
  val DAPH_HOME = System.getenv("DAPH_HOME")

  /**
   * Daph公共目录路径
   */
  val DAPH_DIR_CONFIG = DAPH_HOME + "/conf"
  val DAPH_DIR_DATA = DAPH_HOME + "/data"
  val DAPH_DIR_JARS = DAPH_HOME + "/jars"
  val DAPH_DIR_LOGS = DAPH_HOME + "/logs"
  val DAPH_DIR_EXAMPLES = DAPH_HOME + "/examples"

  /**
   * Daph默认目录路径
   */
  val DEFAULT_DIR_NODE_JARS = DAPH_HOME + "/jars/nodes"

  /**
   * Daph默认文件路径
   */
  val DEFAULT_JSON_EXECUTOR = DAPH_DIR_CONFIG + "/executor.json"
  val DEFAULT_JSON_NODE_DICTIONARY = DAPH_DIR_CONFIG + "/node-dictionary.json"
  val DEFAULT_XML_LOG_XML = DAPH_DIR_CONFIG + "/log4j2.xml"

}
