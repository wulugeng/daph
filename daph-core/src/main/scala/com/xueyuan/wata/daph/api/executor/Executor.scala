package com.xueyuan.wata.daph.api.executor

import com.xueyuan.wata.daph.api.GlobalContext
import com.xueyuan.wata.daph.core.execution.DAG
import com.xueyuan.wata.daph.core.execution.task.Task
import org.apache.logging.log4j.scala.Logging

import java.util.concurrent.atomic.AtomicBoolean

abstract class Executor extends Logging {
  protected var _dag: DAG = _
  protected var _gc: GlobalContext = _
  private val isDone = new AtomicBoolean(false)

  protected final def tasks: Set[Task] = _dag.getTasks

  final def setStatus(b: Boolean): Unit = isDone.set(b)

  final def getStatus = isDone.get

  final def init(dag: DAG, gc: GlobalContext): Unit = {
    _dag = dag
    _gc = gc
  }

  def execute(): Unit

  def stop(): Unit = {}

  def clear(): Unit = {}
}