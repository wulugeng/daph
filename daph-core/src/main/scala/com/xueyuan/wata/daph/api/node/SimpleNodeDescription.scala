package com.xueyuan.wata.daph.api.node

case class SimpleNodeDescription(
  flag: String,
  name: String = "",
  config: Any = None,
  inLines: Array[String] = Array.empty,
  outLines: Array[String] = Array.empty,
  extraOptions: Map[String, String] = Map.empty) {
  override def equals(obj: Any): Boolean = {
    obj match {
      case x: SimpleNodeDescription =>
        flag.equals(x.flag) &
          name.equals(x.name) &
          config.equals(x.config) &
          inLines.sameElements(x.inLines) &
          outLines.sameElements(x.outLines) &
          extraOptions.equals(x.extraOptions)
      case _ => false
    }
  }

  def toPrettyString: String =
    s"""
       |flag: $flag,
       |name: $name,
			 |config: $config,
			 |inLines: ${inLines.mkString("Array(", ", ", ")")},
			 |outLines: ${outLines.mkString("Array(", ", ", ")")},
			 |extraOptions: $extraOptions
			 |""".stripMargin
}
