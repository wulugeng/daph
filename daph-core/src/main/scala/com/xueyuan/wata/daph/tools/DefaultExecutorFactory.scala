package com.xueyuan.wata.daph.tools

import com.xueyuan.wata.daph.api.executor.Executor
import com.xueyuan.wata.daph.core.execution.{BaseFutureExecutor, BasePoolExecutor, ExecutorConfig}
import com.xueyuan.wata.daph.enums.ExecutorType

object DefaultExecutorFactory {
  def getExecutor(config: ExecutorConfig): Executor = {
    ExecutorType.withName(config.exeType) match {
      case ExecutorType.BASE_POOL => new BasePoolExecutor(config.options("parallelism").toInt)
      case ExecutorType.BASE_FUTURE => new BaseFutureExecutor(config.options("parallelism").toInt)
      case ExecutorType.DEFAULT => new BasePoolExecutor(config.options("parallelism").toInt)
      case _ => new BasePoolExecutor(config.options("parallelism").toInt)
    }
  }
}
