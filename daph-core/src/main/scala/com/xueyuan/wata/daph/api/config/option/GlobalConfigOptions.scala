package com.xueyuan.wata.daph.api.config.option

import com.xueyuan.wata.daph.constants.GlobalConstants.{DEFAULT_DIR_NODE_JARS, DEFAULT_JSON_NODE_DICTIONARY, DEFAULT_XML_LOG_XML}

object GlobalConfigOptions {
  /**
   * Job级
   */
  val DAPH_ID = OptionBuilder("daph.id").default()
  val DAPH_INSTANCE_ID = OptionBuilder("daph.instance-id").default()
  val DAPH_NAME = OptionBuilder("daph.name").default()
  val DAPH_DESC = OptionBuilder("daph.description").default()
  val DAPH_USER = OptionBuilder("daph.user").default()
  val DAPH_LOG_LEVEL = OptionBuilder("daph.log-level").default("")
  val DAPH_COMPUTATION_MODE = OptionBuilder("daph.computation-mode").default("batch")
  val DAPH_LOG_XML = OptionBuilder("daph.log-xml").default(DEFAULT_XML_LOG_XML)

  /**
   * 节点级
   */
  val NODE_DICTIONARY_PATH = OptionBuilder("node.dictionary.path").default(DEFAULT_JSON_NODE_DICTIONARY)
  val NODE_JAR_ROOT_PATH = OptionBuilder("node.jar.root.path").default(DEFAULT_DIR_NODE_JARS)
}
