package com.xueyuan.wata.daph.tools

import com.xueyuan.wata.daph.utils.JsonUtil.defaultMapper

import java.io.File
import java.nio.file.Files
import scala.reflect.{ClassTag, classTag}

object JsonTool {
  def loadFile[T: ClassTag](path: String): T = {
    val json = new String(Files.readAllBytes(new File(path).toPath))
    val res = defaultMapper.readValue(json, classTag[T].runtimeClass)
    res.asInstanceOf[T]
  }
}
