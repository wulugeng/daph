package com.xueyuan.wata.daph.core.execution

case class ExecutorConfig(
  exeType: String,
  options: Map[String, String]
)
