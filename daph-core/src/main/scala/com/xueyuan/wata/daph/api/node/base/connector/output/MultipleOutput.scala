package com.xueyuan.wata.daph.api.node.base.connector.output

abstract class MultipleOutput[IN] extends Output {
	final override def run(): Unit = {
		val res = before(lineToDS)
		out(res)
		after()
	}

	protected def before(lineToDS: Map[String, IN]): Map[String, IN] = lineToDS
	protected def out(lineToDS: Map[String, IN]): Unit
	protected def after(): Unit = {}

	final override def viewNode(): Unit = {
		val res = view(lineToDS)
		setDSToLines(res, Array(nodeDescription.id))
	}

	def view(lineToDS: Map[String, IN]): IN = {
		lineToDS.head._2
	}

	private def lineToDS = {
		getLineToDS.map { case (line, ds) => line -> ds.asInstanceOf[IN] }
	}
}
