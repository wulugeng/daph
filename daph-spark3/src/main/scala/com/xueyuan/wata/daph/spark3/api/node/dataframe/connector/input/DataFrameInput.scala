package com.xueyuan.wata.daph.spark3.api.node.dataframe.connector.input

import com.xueyuan.wata.daph.api.node.base.connector.input.Input
import com.xueyuan.wata.daph.spark3.api.node.dataframe.DataFrameNode
import org.apache.spark.sql.DataFrame

abstract class DataFrameInput
  extends Input[DataFrame]
    with DataFrameNode {
  override protected def after(df: DataFrame): DataFrame = {
    executeSQL("afterSQL", df)
  }
}
