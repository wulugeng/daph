package com.xueyuan.wata.daph.spark3.api.node.dataframe.connector.output

import com.xueyuan.wata.daph.api.node.base.connector.output.SingleOutput
import com.xueyuan.wata.daph.spark3.api.node.dataframe.DataFrameNode
import org.apache.spark.sql.DataFrame

abstract class DataFrameSingleOutput
	extends SingleOutput[DataFrame]
		with DataFrameNode {
	override protected def before(df: DataFrame): DataFrame = {
		executeSQL("beforeSQL", df)
	}
}
