package com.xueyuan.wata.daph.spark3.api.node.dataframe.transformer

import com.xueyuan.wata.daph.api.node.base.transformer.SingleTransformer
import com.xueyuan.wata.daph.spark3.api.node.dataframe.DataFrameNode
import org.apache.spark.sql.DataFrame

abstract class DataFrameSingleTransformer extends SingleTransformer[DataFrame, DataFrame]
    with DataFrameNode {
  override protected def before(df: DataFrame): DataFrame = {
    executeSQL("beforeSQL", df)
  }

  override protected def after(df: DataFrame): DataFrame = {
    executeSQL("afterSQL", df)
  }
}
