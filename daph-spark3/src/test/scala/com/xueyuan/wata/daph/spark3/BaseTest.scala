package com.xueyuan.wata.daph.spark3

import com.xueyuan.wata.daph.api.GlobalContext
import com.xueyuan.wata.daph.core.JVMStorage
import com.xueyuan.wata.daph.core.execution.ExecutorConfig
import com.xueyuan.wata.daph.spark3.computer.SparkComputer
import com.xueyuan.wata.daph.tools.DefaultExecutorFactory
import com.xueyuan.wata.daph.tools.jsonloader.{DefaultJsonLoaderFactory, DJson}
import com.xueyuan.wata.daph.utils.JsonUtil.defaultMapper

import scala.language.postfixOps

trait BaseTest {
  final val rootPath: String = System.getProperty("user.dir") + "/src/test/resources/"

  final def executeJob(jobJsonPath: String,
                       computerJsonPath: String = rootPath + "/base/computer.json",
                       storageJsonPath: String = "",
                       executorJsonPath: String = rootPath + "/base/executor.json"): Unit = {
    val jsonPath = DJson(
      jobJsonPath,
      computerJsonPath,
      storageJsonPath,
      executorJsonPath
    )
    val jsonResult = DefaultJsonLoaderFactory.getJsonLoader("local-fs").loadAll(jsonPath)
    val jobJson = jsonResult.job
    val computerJson = jsonResult.computer
    val storageJson = jsonResult.storage
    val executorJson = jsonResult.executor

    val computer = new SparkComputer(computerJson)

    val storage = new JVMStorage

    val exeConfig = defaultMapper.readValue(executorJson, classOf[ExecutorConfig])
    val executor = DefaultExecutorFactory.getExecutor(exeConfig)

    val gc = new GlobalContext(jobJson, computer, storage, executor)
    gc.execute()
    gc.stop()
  }
}