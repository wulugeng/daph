package com.xueyuan.wata.daph.spark3

import com.xueyuan.wata.daph.tools.LogTool
import org.junit.Test

class GeneralTest extends BaseTest {
  val job = rootPath + "jobs/mysql-mysql.json"

  @Test
  def test(): Unit = {
    LogTool.setLogXml("log4j2-test")
    executeJob(job)
  }
}
