<!-- TOC -->
  * [准备](#准备)
  * [使用](#使用)
  * [job说明](#job说明)
    * [job DAG图](#job-dag图)
    * [job.json](#jobjson)
    <!-- TOC -->

## 准备

准备可用的 flink117 环境

## 使用

1. 将daph安装包解压到flink集群任一服务器节点的任意目录
2. 配置daph环境变量DAPH_HOME
3. 将${DAPH_HOME}/jars/computers/flink117目录及子目录内的jar包，根据需求，拷贝到${FLINK_HOME}/lib目录
4. 参照${DAPH_HOME}/examples/jvm/中的json文件，编写job.json与computer.json
5. 运行以下命令启动daph-flink117任务：

```shell
sh ${DAPH_HOME}/bin/daph.sh -a flink117 -b run -t local \
-j ${DAPH_HOME}/examples/flink117/job.json \
-c ${DAPH_HOME}/examples/flink117/computer.json
```

## job说明

### job DAG图

```mermaid
graph LR
    a[mysql-in] --> aa[sql-tr] --> aaa[mysql-out];
```

### job.json

```json
{
  "nodes": [
    {
      "flag": "Flink117.string.general.connector.GeneralInput",
      "config": {
        "catalogRangeType": "global",
        "catalogDatabaseType": "jdbc",
        "catalogName": "jdbc_catalog",
        "databaseName": "wxy",
        "resultTableName": "in_t",
        "resultSql": "select * from in_t1 where f1 = '1'"
      },
      "outLines": [
        "in-line"
      ]
    },
    {
      "flag": "Flink117.string.general.transformer.GeneralSQL",
      "config": {
        "resultTableName": "tmp_t",
        "sqlStatement": "select * from jdbc_catalog.wxy.in_t"
      },
      "inLines": [
        "in-line"
      ],
      "outLines": [
        "sql-line"
      ]
    },
    {
      "flag": "Flink117.string.general.connector.GeneralOutput",
      "config": {
        "catalogRangeType": "global",
        "catalogDatabaseType": "jdbc",
        "catalogName": "jdbc_catalog",
        "databaseName": "wxy",
        "resultTableName": "out_t2",
        "resultSql": "insert into out_t2 select * from default_catalog.default_database.tmp_t"
      },
      "inLines": [
        "sql-line"
      ]
    }
  ]
}
```