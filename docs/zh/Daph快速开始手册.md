<!-- TOC -->
  * [概述](#概述)
  * [启动脚本说明](#启动脚本说明)
    * [daph.sh](#daphsh)
      * [概述](#概述-1)
      * [详述](#详述)
    * [daph-jvm.sh](#daph-jvmsh)
    * [daph-jvm-spark3.sh](#daph-jvm-spark3sh)
    * [daph-jvm-flink117.sh](#daph-jvm-flink117sh)
    * [daph-spark3.sh](#daph-spark3sh)
    * [daph-flink117.sh](#daph-flink117sh)
  * [json文件说明](#json文件说明)
    * [1）job.json](#1jobjson)
      * [概述](#概述-2)
      * [详述](#详述-1)
        * [job.json对应的DAG图](#jobjson对应的dag图)
        * [job.json对应的文件内容](#jobjson对应的文件内容)
    * [2）computer.json](#2computerjson)
      * [概述](#概述-3)
      * [详述](#详述-2)
    * [3）storage.json](#3storagejson)
    * [4）executor.json](#4executorjson)
      * [概述](#概述-4)
      * [详述](#详述-3)
<!-- TOC -->

## 概述

分别以daph-jvm、daph-spark3、daph-flink117为例。

- daph-jvm见[Daph-jvm快速开始手册](Daph-jvm快速开始手册.md)
- daph-spark3见[Daph-spark3快速开始手册](Daph-spark3快速开始手册.md)
- daph-flink117见[Daph-flink117快速开始手册](Daph-flink117快速开始手册.md)

## 启动脚本说明

### daph.sh
#### 概述
daph.sh是Daph启动总入口，可启动所有Daph计算器。
#### 详述
以启动Daph-flink117计算器为例：
```shell
sh ${DAPH_HOME}/bin/daph.sh \
# 必填项。指定要启动的计算器，可选项有jvm/jvm-spark3/jvm-flink117/spark3/flink117
-a flink117 \
# flink计算器特有参数
-b run \
# flink计算器特有参数
-t local \
# 必填项。同--job。指定job.json
-j ${DAPH_HOME}/examples/flink117/job.json \
# jvm没有此项，其他计算器必填项。同--computer。指定computer.json
-c ${DAPH_HOME}/examples/flink117/computer.json \
# 可选项。同--executor。指定executor.json
-e ${DAPH_HOME}/examples/flink117/executor.json
# 可选项。同--dudps。指定日志文件，及指定自定义参数
-d logXml=${DAPH_HOME}/conf/log4j2-flink117.xml
```
### daph-jvm.sh
daph-jvm.sh是Daph-jvm计算器启动入口。

```shell
sh ${DAPH_HOME}/bin/daph-jvm.sh \
-j ${DAPH_HOME}/examples/jvm/job.json
```
### daph-jvm-spark3.sh
daph-jvm-spark3.sh是Daph-spark3计算器启动入口。<br>
该脚本是以原生java命令启动spark任务，类似IDEA中运行spark任务。

```shell
sh ${DAPH_HOME}/bin/daph-jvm-spark3.sh \
-j ${DAPH_HOME}/examples/spark3/job.json \
-c ${DAPH_HOME}/examples/spark3/computer.json
```
### daph-jvm-flink117.sh
daph-jvm-flink117.sh是Daph-flink117计算器启动入口。<br>
该脚本是以原生java命令启动flink任务，类似IDEA中运行flink任务。

```shell
sh ${DAPH_HOME}/bin/daph-jvm-flink117.sh \
-j ${DAPH_HOME}/examples/flink117/job.json \
-c ${DAPH_HOME}/examples/flink117/computer.json
```
### daph-spark3.sh
daph-spark3.sh是Daph-spark3计算器启动入口。

```shell
sh ${DAPH_HOME}/bin/daph-spark3.sh \
-j ${DAPH_HOME}/examples/spark3/job.json \
-c ${DAPH_HOME}/examples/spark3/computer.json
```
### daph-flink117.sh
daph-flink117.sh是Daph-flink117计算器启动入口。

```shell
sh ${DAPH_HOME}/bin/daph-flink117.sh \
-b run \
-t local \
-j ${DAPH_HOME}/examples/flink117/job.json \
-c ${DAPH_HOME}/examples/flink117/computer.json
```

## json文件说明

### 1）job.json

#### 概述

job.json文件是关于daph job的配置文件。<br>
job.json文件是必须的配置文件。

#### 详述

##### job.json对应的DAG图

```mermaid
graph LR
    a1[Input] --> aa2[Transformer] --> aaa1[Output];
```

##### job.json对应的文件内容

```json
{
  // job全局配置项，可不填写，均有默认值。
  // 若是yarn或k8s模式，则必须填写以下两个配置项。
  "options": {
    // 节点字典文件路径，默认是daph/config/node-dictionary.json
    "node.dictionary.path": "${DAPH_HOME}/config/node-dictionary.json",
    // 节点jar文件所在目录，默认是daph/jars/nodes/
    "node.jar.root.path": "${DAPH_HOME}/jars/nodes/"
  },
  // job节点配置项，必填项
  "nodes": [
    {
      // 节点标识。更多节点标识，见节点字典文件
      "flag": "计算器名称.数据结构名称.流批类型.节点类型.Input",
      // 节点名称。可不填写，默认值为空字符串
      "name": "",
      // 节点配置。不同的节点，配置不同，见各个计算器的节点手册
      "config": {},
      // 节点输出线。构成节点之间的关系
      "outLines": [
        "in-line"
      ]
    },
    {
      "flag": "计算器名称.数据结构名称.流批类型.节点类型.Transformer",
      "config": {},
      "inLines": [
        "in-line"
      ],
      "outLines": [
        "tr-line"
      ]
    },
    {
      "flag": "计算器名称.数据结构名称.流批类型.节点类型.Output",
      "config": {},
      // 节点输入线。构成节点之间的关系
      "inLines": [
        "in-line"
      ]
    }
  ]
}
```

### 2）computer.json

#### 概述

computer.json文件是关于daph计算器的配置文件，其中的配置项可参考各个计算器源码工程中的计算器配置类。<br>
computer.json文件不是必须的配置文件，比如daph-jvm计算器目前就没有规划computer.json文件。<br>
比如daph-spark3计算器配置类具有envConfig与methodConfig两个参数，<br>
其中envConfig应接收的参数，即spark配置参数。

#### 详述

```json
{
  // Spark配置项。此处的配置项均会设置到SparkConf
  "envConfig": {
    "spark.master": "local[*]",
    "spark.app.name": "Spark Example",
    "spark.sql.warehouse.dir": "warehouse",
    "spark.ui.enabled": "false"
  }
}
```

### 3）storage.json

storage.json文件是关于daph-core的数据结构存储器的配置文件。<br>
storage.json文件不是必须的配置文件，目前没有规划storage.json文件。

### 4）executor.json

#### 概述

executor.json文件是关于daph-core的DAG执行器的配置文件。<br>
executor.json文件不是必须的配置文件，一般不用配置，也不用修改配置，可适当调节并行度。

#### 详述

```json
{
  // 目前仅支持一种通用DAG执行器，后续会支持多种用于不同DAG模型的执行器
  "exeType": "default",
  // 节点执行并行度。调节为3，就能够同时执行3个节点。对于daph-spark，可适当调节；对于daph-flink，并行度必须为1
  "options": {
    "parallelism": "1"
  }
}
```