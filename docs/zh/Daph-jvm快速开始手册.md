<!-- TOC -->
  * [准备](#准备)
  * [使用](#使用)
  * [job说明](#job说明)
    * [job DAG图](#job-dag图)
    * [job.json](#jobjson)
    <!-- TOC -->

## 准备

准备可用的jdk8环境

## 使用

1. 将daph安装包解压到jdk集群任一服务器节点的任意目录
2. 配置daph环境变量DAPH_HOME
3. 参照daph/examples/jvm/中的json文件，编写job.json
4. 运行以下命令启动daph-jvm任务：

```shell
sh ${DAPH_HOME}/bin/daph.sh -a jvm \
-j ${DAPH_HOME}/examples/jvm/job.json
```

## job说明

### job DAG图

```mermaid
graph LR
    a[http-in] --> aa[http-tr] --> aaa[http-out];
```

### job.json

```json
{
  "nodes": [
    {
      "flag": "JVM.httpresult.simple.connector.HttpInput",
      "config": {
        "url": "https://cn.bing.com/search?q=w"
      },
      "outLines": [
        "in-line"
      ]
    },
    {
      "flag": "JVM.httpresult.simple.transformer.HttpTransformer",
      "config": {
        "url": "https://cn.bing.com/search?q=x"
      },
      "inLines": [
        "in-line"
      ],
      "outLines": [
        "tr-line"
      ]
    },
    {
      "flag": "JVM.httpresult.simple.connector.HttpOutput",
      "config": {
        "url": "https://cn.bing.com/search?q=y"
      },
      "inLines": [
        "tr-line"
      ]
    }
  ]
}
```