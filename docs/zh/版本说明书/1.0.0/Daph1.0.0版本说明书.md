<!-- TOC -->
  * [内容概况](#内容概况)
  * [功能列表](#功能列表)
  * [节点矩阵](#节点矩阵)
<!-- TOC -->
## 内容概况

Daph1.0.0是Daph的第一个修订版本。<br>
Daph1.0.0虽然是Daph的第一个修订版本，但功能已经很强悍。<br>
Daph1.0.0已经具有强大的DAG流水线能力，已经具有任意数据结构接入能力，已经引入JVM/Spark3/Flink117计算引擎。

## 功能列表

- **DAG流水线能力**
- **数据结构接入能力**
- **计算引擎接入能力**
- **多数据源连接能力**
- **多种通用数据转换能力**

## 节点矩阵

- **daph-jvm**：见[Daph-jvm节点手册](nodes/jvm/Daph-jvm节点手册.md)
- **daph-spark3**：见[Daph-spark3节点手册](nodes/spark3/Daph-spark3节点手册.md)
- **daph-flink117**：见[Daph-flink117节点手册](nodes/flink117/Daph-flink117节点手册.md)
