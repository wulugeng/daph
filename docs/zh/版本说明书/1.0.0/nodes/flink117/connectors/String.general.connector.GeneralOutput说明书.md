<!-- TOC -->

* [简介](#简介)
* [配置项](#配置项)
    * [节点配置项](#节点配置项)
* [使用案例](#使用案例)
    * [DAG图](#dag图)
    * [job.json](#jobjson)

<!-- TOC -->

## 简介

- **节点标识**：Flink117.string.general.connector.GeneralOutput
- **简单标识**：flink117-sql-out
- **节点类型**：输出节点
- **节点功能**：将TableEnv中的一个表，写出到一个外部数据表或文件
- **流批类型**：流批
- **支持的数据源类型**
  ：请参考[flink-docs-release-1.17/docs/connectors/table](https://nightlies.apache.org/flink/flink-docs-release-1.17/docs/connectors/table/overview/)

## 配置项

### 节点配置项

| 配置名称                | 配置类型               | 是否必填项 | 默认值              | 描述                                                                                                                                                                                                                                                                                                                                |
|---------------------|--------------------|-------|------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| catalogRangeType    | String             | 否     | default          | 若未配置createSql与resultSql，则必填。global：全局catalog<br/>local：本节点catalog<br/>default：flink默认catalog                                                                                                                                                                                                                                      |
| catalogDatabaseType | String             | 否     | -                | hive/jdbc/hudi/iceberg                                                                                                                                                                                                                                                                                                            |
| catalogName         | String             | 否     | default_catalog  | catalog名称                                                                                                                                                                                                                                                                                                                         |
| catalogConfig       | Map[String,String] | 否     | -                | 请参考[flink-docs-release-1.17/docs/connectors/table](https://nightlies.apache.org/flink/flink-docs-release-1.17/docs/connectors/table/overview/)                                                                                                                                                                                    |
| databaseName        | String             | 否     | default_database | catalog数据库名称                                                                                                                                                                                                                                                                                                                      |
| sqlDialect          | String             | 否     | default          | default/hive                                                                                                                                                                                                                                                                                                                      |
| createSql           | String             | 否     | -                | 若未配置catalog，则必填。表创建语句。若未指定catalog或database，则createSql中的表名格式必须是catalog名.数据库名.表名<br/>请参考[flink-docs-release-1.17/docs/connectors/table](https://nightlies.apache.org/flink/flink-docs-release-1.17/docs/connectors/table/overview/)<br/>若未指定catalog，则createSql中的表名格式必须是catalog名.数据库名.表名<br/>若未指定database，则createSql中的表名格式必须是数据库名.表名 |
| resultSql           | String             | 是     | -                | 若未配置catalog，则必填。插入语句。若未指定catalog或database，则resultSql中的表名格式必须是catalog名.数据库名.表名                                                                                                                                                                                                                                                     |

## 使用案例

### DAG图

```mermaid
graph LR
    a[mysql-in] --> aa[sql-tr] --> aaa[mysql-out];
```

### job.json

```json
{
  "nodes": [
    {
      "flag": "Flink117.string.general.connector.GeneralInput",
      "config": {
        "catalogRangeType": "global",
        "catalogDatabaseType": "jdbc",
        "catalogName": "jdbc_catalog",
        "databaseName": "wxy",
        "resultTableName": "in_t",
        "resultSql": "select * from in_t1 where f1 = '1'"
      },
      "outLines": [
        "in-line"
      ]
    },
    {
      "flag": "Flink117.string.general.transformer.GeneralSQL",
      "config": {
        "resultTableName": "tmp_t",
        "sqlStatement": "select * from jdbc_catalog.wxy.in_t"
      },
      "inLines": [
        "in-line"
      ],
      "outLines": [
        "sql-line"
      ]
    },
    {
      "flag": "Flink117.string.general.connector.GeneralOutput",
      "config": {
        "catalogRangeType": "global",
        "catalogDatabaseType": "jdbc",
        "catalogName": "jdbc_catalog",
        "databaseName": "wxy",
        "resultSql": "insert into out_t2 select * from default_catalog.default_database.tmp_t"
      },
      "inLines": [
        "sql-line"
      ]
    }
  ]
}
```
