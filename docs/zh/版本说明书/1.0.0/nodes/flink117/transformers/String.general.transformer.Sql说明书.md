## 简介

- **节点标识**：Flink117.string.general.transformer.Sql
- **简单标识**：flink117-sql-tr
- **节点类型**：转换节点
- **节点功能**：基于截止上游节点时的TableEnv，执行sql，更新TableEnv，以供下游节点使用最新的TableEnv
- **流批类型**：流批

## 输入输出数据结构

| 输入数据结构   | 输出数据结构   |
|----------|----------|
| 多个String | 一个String |

## 配置项

| 配置名称            | 配置类型   | 是否必填项 | 默认值 | 描述    |
|-----------------|--------|-------|-----|-------|
| resultTableName | String | 是     | -   | 结果表名称 |
| sqlStatement    | String | 是     | -   | sql语句 |

## 使用案例

### DAG图

```mermaid
graph LR
    a1[mysql-in1] --> aa[sql-tr];
    a2[mysql-in2] --> aa[sql-tr];
    aa[sql-tr] --> aaa[mysql-out];
```

### job.json

```json
{
  "nodes": [
    {
      "flag": "Flink117.string.general.connector.GeneralInput",
      "config": {
        "catalogRangeType": "global",
        "catalogDatabaseType": "jdbc",
        "catalogName": "jdbc_catalog",
        "databaseName": "wxy",
        "resultTableName": "in_t1",
        "resultSql": "select * from in_t1 where f1 = '1'"
      },
      "outLines": [
        "in-line1"
      ]
    },
    {
      "flag": "Flink117.string.general.connector.GeneralInput",
      "config": {
        "catalogRangeType": "global",
        "catalogDatabaseType": "jdbc",
        "catalogName": "jdbc_catalog",
        "databaseName": "wxy",
        "resultTableName": "in_t2",
        "resultSql": "select * from in_t2 where f1 = '1'"
      },
      "outLines": [
        "in-line2"
      ]
    },
    {
      "flag": "Flink117.string.general.connector.GeneralSql",
      "config": {
        "resultTableName": "tmp_t",
        "sqlStatement": "select * from jdbc_catalog.wxy.in_t1 as a join jdbc_catalog.wxy.in_t2 as b on a.f1 = b.f1"
      },
      "inLines": [
        "in-line1",
        "in-line2"
      ],
      "outLines": [
        "sql-line"
      ]
    },
    {
      "flag": "Flink117.string.general.connector.GeneralOutput",
      "config": {
        "catalogRangeType": "global",
        "catalogDatabaseType": "jdbc",
        "catalogName": "jdbc_catalog",
        "databaseName": "wxy",
        "resultSql": "insert into out_t2 select * from default_catalog.default_database.tmp_t"
      },
      "inLines": [
        "sql-line"
      ]
    }
  ]
}
```
