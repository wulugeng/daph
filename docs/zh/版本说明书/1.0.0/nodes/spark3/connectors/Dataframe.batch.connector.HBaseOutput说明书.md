## 简介

- **节点标识**：Spark3.dataframe.batch.connector.HBaseOutput
- **简单标识**：spark3-batch-hbase-out
- **节点类型**：输出节点
- **节点功能**：接收一个DataFrame，将DataFrame承载的数据写出到一个HBase表
- **流批类型**：批

## 配置项

| 配置名称         | 配置类型               | 是否必填项 | 默认值    | 描述                                                                             |
|--------------|--------------------|-------|--------|--------------------------------------------------------------------------------|
| stagingDir   | String             | 是     | -      | HFileOutputFormat的输出路径                                                         |
| saveMode     | String             | 否     | append | append, overwrite                                                              |
| catalog      | String             | 是     | -      | 请参考[hbase spark](https://github.com/apache/hbase-connectors/tree/master/spark) |
| hbaseOptions | Map[String,String] | 是     | -      | 请参考[hbase spark](https://github.com/apache/hbase-connectors/tree/master/spark) |

## 使用案例

### DAG图

```mermaid
graph LR
    a[es-in] --> aa[hbase-out];
```

### job.json

```json
{
  "nodes": [
    {
      "flag": "Spark3.dataframe.batch.connector.CommonInput",
      "config": {
        "format": "es",
        "cfg": {
          "es.nodes": "127.0.0.1:9200,127.0.0.2:9200"
        }
      },
      "outLines": [
        "in-line"
      ]
    },
    {
      "flag": "Spark3.dataframe.batch.connector.HBaseOutput",
      "config": {
        "stagingDir": "/daph/hbase/",
        "saveMode": "overwrite",
        "catalog": "hbase",
        "hbaseOptions": {}
      },
      "inLines": [
        "in-line"
      ]
    }
  ]
}
```
