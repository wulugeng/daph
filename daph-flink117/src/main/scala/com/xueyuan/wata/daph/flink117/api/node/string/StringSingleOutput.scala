package com.xueyuan.wata.daph.flink117.api.node.string

import com.xueyuan.wata.daph.api.node.base.connector.output.SingleOutput
import com.xueyuan.wata.daph.flink117.api.node.datastream.DataStreamNode

abstract class StringSingleOutput extends SingleOutput[String]
  with DataStreamNode {
  override protected def before(ds: String) = {
    super.before(ds)
  }

  override protected def after(): Unit = {
    super.after()
  }
}
