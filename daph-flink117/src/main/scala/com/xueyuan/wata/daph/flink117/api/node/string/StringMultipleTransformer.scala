package com.xueyuan.wata.daph.flink117.api.node.string

import com.xueyuan.wata.daph.api.node.base.transformer.MultipleTransformer
import com.xueyuan.wata.daph.flink117.api.node.datastream.DataStreamNode

abstract class StringMultipleTransformer
	extends MultipleTransformer[String, String]
		with DataStreamNode{
	override protected def before(lineToDS: Map[String, String]) = {
		super.before(lineToDS)
	}

	override protected def after(out: String) = {
		super.after(out)
	}
}