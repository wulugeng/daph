package com.xueyuan.wata.daph.flink117.api.node.datastream

import com.xueyuan.wata.daph.api.node.base.transformer.SingleTransformer
import org.apache.flink.streaming.api.datastream.DataStream

abstract class DataStreamSingleTransformer[IN, OUT]
	extends SingleTransformer[DataStream[IN], DataStream[OUT]]
		with DataStreamNode
