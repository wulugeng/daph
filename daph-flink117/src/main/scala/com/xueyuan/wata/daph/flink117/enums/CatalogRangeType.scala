package com.xueyuan.wata.daph.flink117.enums

object CatalogRangeType extends Enumeration {
  val GLOBAL = Value(name = "global")
  val LOCAL = Value(name = "local")
  val DEFAULT = Value(name = "default")
}
