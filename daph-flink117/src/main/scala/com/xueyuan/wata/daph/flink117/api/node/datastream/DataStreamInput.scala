package com.xueyuan.wata.daph.flink117.api.node.datastream

import com.xueyuan.wata.daph.api.node.base.connector.input.Input
import org.apache.flink.streaming.api.datastream.DataStream

abstract class DataStreamInput[OUT]
  extends Input[DataStream[OUT]]
    with DataStreamNode