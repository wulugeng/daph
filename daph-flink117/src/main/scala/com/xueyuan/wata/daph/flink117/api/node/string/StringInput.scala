package com.xueyuan.wata.daph.flink117.api.node.string

import com.xueyuan.wata.daph.api.node.base.connector.input.Input
import com.xueyuan.wata.daph.flink117.api.node.datastream.DataStreamNode

abstract class StringInput extends Input[String]
  with DataStreamNode