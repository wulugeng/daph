package com.xueyuan.wata.daph.flink117.api.node.string

import com.xueyuan.wata.daph.api.node.base.connector.output.MultipleOutput
import com.xueyuan.wata.daph.flink117.api.node.datastream.DataStreamNode

abstract class StringMultipleOutput extends MultipleOutput[String]
  with DataStreamNode
