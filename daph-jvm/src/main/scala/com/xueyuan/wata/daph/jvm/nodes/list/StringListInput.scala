package com.xueyuan.wata.daph.jvm.nodes.list

import com.xueyuan.wata.daph.jvm.api.node.list.ListInput

class StringListInput extends ListInput[String] {
  override protected def in(): List[String] = {
    List("daph-jvm-list-string-data")
  }
}
