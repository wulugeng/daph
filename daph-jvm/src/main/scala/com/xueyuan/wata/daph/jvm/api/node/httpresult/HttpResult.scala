package com.xueyuan.wata.daph.jvm.api.node.httpresult

case class HttpResult(
  code: Int = 500,
  message: String = "Internal Server Error"
)
