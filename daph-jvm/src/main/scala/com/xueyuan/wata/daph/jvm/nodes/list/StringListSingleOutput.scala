package com.xueyuan.wata.daph.jvm.nodes.list

import com.xueyuan.wata.daph.jvm.api.node.list.ListSingleOutput

class StringListSingleOutput extends ListSingleOutput[String] {
  override protected def out(ds: List[String]): Unit = {
    println(ds.mkString)
  }
}
