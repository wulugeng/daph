package com.xueyuan.wata.daph.jvm.api.node.list

import com.xueyuan.wata.daph.api.node.base.transformer.SingleTransformer

abstract class ListSingleTransformer[IN, OUT]
  extends SingleTransformer[List[IN], List[OUT]]
