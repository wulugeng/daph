package com.xueyuan.wata.daph.jvm.api.node.bool

import com.xueyuan.wata.daph.api.node.base.connector.output.SingleOutput

abstract class BoolSingleOutput
  extends SingleOutput[Boolean]
