package com.xueyuan.wata.daph.jvm.api.node.list

import com.xueyuan.wata.daph.api.node.base.connector.output.SingleOutput

abstract class ListSingleOutput[OUT]
  extends SingleOutput[List[OUT]]
