package com.xueyuan.wata.daph.jvm.api.node.httpresult

import com.xueyuan.wata.daph.api.node.base.transformer.SingleTransformer

abstract class HttpResultSingleTransformer
  extends SingleTransformer[HttpResult, HttpResult]
