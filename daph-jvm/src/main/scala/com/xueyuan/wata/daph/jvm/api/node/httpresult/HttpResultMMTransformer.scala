package com.xueyuan.wata.daph.jvm.api.node.httpresult

import com.xueyuan.wata.daph.api.node.base.transformer.MMTransformer

abstract class HttpResultMMTransformer
  extends MMTransformer[HttpResult, HttpResult]
